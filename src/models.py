""" Data layer """

import json
import re
import hashlib
import copy
from datetime import datetime, date

import settings
import func
import commands


def allowed_tagname(value):
    """ Only such tag names are allowed """
    value = value or ''  # avoid none
    return True if re.match(r'^[A-Z]{1,1}[A-Z_\-0-9]{1,99}$', value) else False


def defaultsort(x):
    """ Defines default whip sorting: Todays first, than by priority, than by date.
        Works fine for whips younger than 28 years"""
    return -(int(x.is_today) * 100000 + x.priority * 10000 + (10000-x.age_in_days()))


def serialized(obj):
    """ Returns dictionary with all properties of object obj
        ommits callable attributes (=methods) and hidden properties (anything starting with '_')
    """
    ret = {}
    for attr in dir(obj):
        if not attr.startswith('_'):
            value = getattr(obj, attr)
            if not callable(value):
                ret[attr] = value
    return ret


# names of Whip attributes saved and set only under certain conditions
OPTIONAL_ATTRIBUTES = ("wait_condition", "wait_condition_value")


class Whip:
    """ Basic data structure for one todo list item (=whip)
        Meaning of some properties:
            sync_data: hahtable set by Importer, containing
                       custom values necessary for syncing with whip origin
                       (f.e. gitlab issue). Should contain enough info to sync 
                       whip, and be API-independed (e.g. do not store API url here)
                       In sync_data should be the field 'source', 'cause is used
                       when asking user if he wants to sync back
            autosync:  string "yes", "no" or None
                       if whip should be auto synced back to source ,using sync_data
    """

    def __init__(self, data=None, md5=None):

        def _datavalue(name, default):
            try:
                return data[name]
            except (KeyError, TypeError):
                return default

        self.sync_data = _datavalue('sync_data', {})
        self.autosync = _datavalue('autosync', None)

        # must be set before 'message' property
        if md5:
            self.__md5 = md5
        self.message = _datavalue('message', '')
        # todo prevadet hned na timestamp
        self.timestamp = _datavalue('timestamp', datetime.timestamp(datetime.now()))
        self.__tags = _datavalue('tags', [])
        self.priority = _datavalue('priority', 0)
        self.status = _datavalue('status', 'active')
        self.publish_slug = _datavalue('publish_slug', '')

        # migration from versions below 0.10
        try:
            self.add_tag(_datavalue('group', None))
        except ValueError:
            pass

        for x in OPTIONAL_ATTRIBUTES:
            try:
                val = data[x]
            except (KeyError, TypeError):
                continue
            setattr(self, x, val)

    def transport(self):
        """ Returns typle: (data, hash) 
            data .. whip data converted to JSON string, which can be sent to sync server.
            hash .. sha256 hash of data
        """
        ret = serialized(self)
        # python dictionary is unsorted, so some sorting (here by key names) is necessary,
        # otherwise sha256 yields different results on the same data
        ret = {k: v for k, v in sorted(ret.items(), key=lambda item: str(item[0]))}
        ret = json.dumps(ret)
        myhash = hashlib.sha256(ret.encode('utf-8')).hexdigest()
        return ret, myhash

    @property
    def is_today(self):
        """ Tryies to quess from whip text if whip is scheduled for today 
            today's whips are shown inverted, in front of the list
            Pretty ugly. Should be made with regexep strings, on day...
        """
        day = datetime.today().day
        month = datetime.today().month
        if "%s.%s." % (day, month) in self.message: return True  # European style of date
        if "%s.%s" % (day, month) in self.message: return True
        if "%s-%s" % (month, day) in self.message: return True  # US - as i hope :-) - style of dates
        if "0%s-%s" % (month, day) in self.message: return True
        if "%s/%s" % (month, day) in self.message: return True
        if "0%s/%s" % (month, day) in self.message: return True
        return False

    @property
    def md5(self):
        try:
            return self.__md5
        except AttributeError:
            return None

    @property
    def priority(self):
        return self.__priority

    @priority.setter
    def priority(self, value):
        if value in range(0, 4):
            self.__priority = value
        else:
            raise ValueError('priority not in range 0-3')

    @property
    def public_url(self):
        try:
            slug = self.publish_slug
        except AttributeError:
            return ""
        if slug:
            return "https://whiphub.com/pub/youremail/%s" % ( (self.publish_slug + "/") if self.publish_slug!="*" else "")

    @property
    def autosync(self):
        return self.__autosync

    @autosync.setter
    def autosync(self, value):
        if value in (None, "yes", "no"):
            self.__autosync = value
        else:
            raise ValueError("""autosync not in 'yes', 'no' or None""")

    @property
    def status(self):
        return self.__status

    @status.setter
    def status(self, value):
        if value in ('active', 'deleted', 'waiting'):
            self.__status = value
        else:
            raise ValueError('Unknown status %s' % value)

    @property
    def tags(self):
        return list(set(self.__tags))

    def add_tag(self, value):
        if allowed_tagname(value):
            if value != settings.NO_GROUP_NAME:
                self.__tags.append(value)
        else:
            raise ValueError("""Value "%s" not allowed as tag name""" % value)

    def delete_tag(self, value):
        while value in self.__tags:
            self.__tags.remove(value)

    @property
    def message(self):
        return self.__message

    @message.setter
    def message(self, value):
        self.__message = value
        if value and not self.md5:
            # count md5. Should be unique "worldwide", so add some salt to whip text to distinguish.
            md5_base = value + func.random_string(100)
            self.__md5 = hashlib.md5(md5_base.encode('utf-8')).hexdigest()

    def age_in_days(self):
        delta = date.today() - date.fromtimestamp(self.timestamp)
        return delta.days

    def nicedate(self):
        days = self.age_in_days()
        if days == 0:
            return "Today"
        elif days == 1:
            return "Ystdy"
        else:
            return ("      %s d" % days)[-5:]

    def wait_until_timestamp(self, timestamp):
        """ Sets whip to waiting state, until timestamp is met """
        self.status = 'waiting'
        self.wait_condition = 'until_timestamp'
        self.wait_condition_value = str(int(timestamp))

    def wait_remaining_days(self):
        """ Returns number of days, until Whip should wait. If there is no such condition, return None"""
        try:
            delta = date.fromtimestamp(int(self.wait_condition_value)) - date.today()
        except AttributeError:
            return None
        return delta.days

    @property
    def nice_wait_condition(self):
        """ Returns string with (brief) explanation, why and until what condition Whip waits"""
        days = self.wait_remaining_days()
        return "till ?" if days is None else "%s days" % days


class Whips:
    def __init__(self, parent):
        self.parent = parent
        """ self.__whips is organized in hashtable, key is md5,
            values are unsorted list of whips (=history of whip)
        """
        self.__whips = {}

    @property
    def data(self):
        """ Returns formatted data for save """
        ret = {}
        for key in self.__whips:
            ret[key] = [serialized(x) for x in self.__whips[key]]
        return ret

    @property
    def really_all(self):
        """ returns unsorted list of whips of any status"""
        ret = []
        for key in self.__whips:
            ret.extend(self.__whips[key])
        return ret

    def newest(self):
        """ returns hahtable {md5:newest_whip} for all whips, including deleted """
        ret = {}
        for key in self.__whips:
            ret[key] = sorted(self.__whips[key], key=lambda x: -x.timestamp)[0]
        return ret

    @property
    def active(self):
        """ returns list of all active whips, todays whips first, than sorted by priority."""
        return sorted([x for x in self.newest().values() if x.status == 'active'], key=defaultsort)

    @property
    def waiting(self):
        """ returns list of all waiting whips, todays whips first, than sorted by priority."""
        return sorted([x for x in self.newest().values() if x.status == 'waiting'], key=defaultsort)

    @property
    def undeleted(self):
        """ returns list of all undeleted whips, todays whips first, than sorted by priority."""
        return sorted([x for x in self.newest().values() if x.status != 'deleted'], key=defaultsort)

    def get(self, md5):
        """ returns undeleted whip definded by md5 """
        x = list(filter(lambda x: md5 == x.md5, self.undeleted))
        return x[0] if x else None

    def _get_clone(self, md5):
        """ Returns clone of whip with md5 (if such exists) with a new timestamp
            otherwise new whip.
            Reason: Every modification to whip should be made on such clone
        """
        if md5:
            whip = self.get(md5)
            if whip:
                ret = copy.deepcopy(whip)
                ret.timestamp = datetime.timestamp(datetime.now())
            else:
                raise ValueError("Whip with md5=%s does not exist" % md5)
        else:
            ret = Whip()
        return ret

    def modify(self, md5, modifier_func):
        """ Modifies whip the standard way: by adding modified whip clone 
            modifier_func takes one parameter: modified whip
            In modified whip, the attribute .modified=True is set, to be used in save() function,
            so it can save only modified whips.
            ContextManaget ensures deleting "modified" attributes after each load/save operation.
            Returns modified whip
        """

        w = self._get_clone(md5)
        old_tag_list = w.tags
        modifier_func(w)
        w.modified = True
        self.add(w)

        # There is also the place to call SmartTag action, if some tags were added during modification
        for tag in w.tags:
            if tag not in old_tag_list:
                for st in self.parent.tags.smart_tags:
                    if st.name.upper() == tag.upper():
                        st.action_do(self.parent, w)

        return w

    def add(self, whip):
        if whip.md5 in self.__whips.keys():
            self.__whips[whip.md5].append(whip)
        else:
            self.__whips[whip.md5] = [whip]

    def delete(self, md5):
        """ delete entry specified by md5. Every modification, including deletion, 
            is made as adding a modified clone of old whip
        """
        whip = self._get_clone(md5)
        whip.status = 'deleted'
        self.add(whip)

    def check_waiting(self):
        """ Checks, if whips still wait (if not, set back to active state)
            returns, if something was modified
        """
        def modifier(w):
            try:
                delattr(w, 'wait_condition')
                delattr(w, 'wait_condition_value')
            except AttributeError:
                pass
            w.status = 'active'

        ret = False
        for whip in self.waiting:
            if whip.status == 'waiting':
                if whip.wait_remaining_days() <= 0:
                    self.modify(whip.md5, modifier)
                    ret = True

        return ret


class ContextManager:
    """ Manages all context, specially collection of entries """

    def __init__(self, loadfunc, savefunc):
        self.__last_view_mapping = {}
        self.whips = Whips(self)

        loadfunc(self)
        self._clean_modified_attributes()

        self.tags = Tags(self)
        self.verbose = False
        self.guessed_tagname = None
        self.savefunc = savefunc  # set it last , to prevent autosaving during load
        self.reversed_homepage = False  # for functionality "Enter changes homepage ordering"

    def _clean_modified_attributes(self):
        """ Whip attribute .modified is set after each modification.
            Can be used by save() function to save only changed whips.
            After each load or save, all .modified attributes must be deleted.
        """
        for w in self.whips.really_all:
            try:
                del w.modified
            except AttributeError:
                pass

    def save(self):
        self.savefunc(self)
        self._clean_modified_attributes()

    def set_last_view_mapping(self, mapping):
        self.__last_view_mapping = mapping

    def get_whip_by_userspec(self, arg):
        """ Returns md5 of undeleted (active or waiting) whip, identified by m5  or index in last list
            (if arg is in form of md5, assume it is md5 of a whip, othervise order)
            If not found, returns None
            If ambiguous, prints error and returns None
        """
        arg = arg.strip()
        if arg:
            if re.match(r'^[0-9a-f]{32,32}$', arg):
                return arg
            elif re.match(r'^[0-9]{1,2}$', arg) and arg in self.__last_view_mapping.keys():
                return self.__last_view_mapping[arg]


class Tags:
    """ This is kind of 'virtual' model. Tag data ere not stored separatelly,
        but in entries stored in owner (class ContextManager)
    """

    def __init__(self, parent):
        self.owner = parent

        # Smart tags are hardcoded for now
        self.smart_tags = [
            SmartTag(name='finished', action="w $ 7", emoticon='\U0001F4A4', desc='Finished tasks. Auto wait 1 week'),
            # SmartTag(name='public', action="p $ public", emoticon='\U0001F310'),
        ]

    def _get_whips(self):
        if self.owner:
            return self.owner.whips.undeleted
        else:
            print('error no owner set')
            return []

    def list(self):
        """ returns list of SmartTags
            Some are hardcoded, if there is no hardcoded SmartTag, there is created now with no action
        """
        initial = [settings.NO_GROUP_NAME]
        for x in self._get_whips():
            initial.extend(x.tags)
        tags_from_whips = sorted(list(set(filter(lambda x: x, initial))))

        ret = copy.copy(self.smart_tags)
        for tag in tags_from_whips:
            if not len(list(filter(lambda x: x.name.upper() == tag.upper(), self.smart_tags))):
                ret.append(SmartTag(name=tag, action=""))

        return ret

    def guess(self, name_fraction):
        """ tryies to guess tag by fraction of its's name, which can be just a few starting letters.
            If not ambiquious, returns matching tag name, othervise None
            Example: Assume there are tags ['HOME, 'HELLO']. Than calling:
                guess('H')  .. returns None
                guess('HO') .. returns 'HOME'
        """
        matching = [x for x in self.list() if x.name.startswith(name_fraction)]
        return matching[0] if len(matching) == 1 else None

    def get(self, tag_name):
        """ returns list of undeleted whips with tag or no group"""
        try:
            tag_name = str(tag_name)
        except ValueError:
            tag_name = ''
        if tag_name == settings.NO_GROUP_NAME:
            filter_func = lambda x: not x.tags
        else:
            filter_func = lambda x: tag_name.upper() in x.tags
        return [x for x in self._get_whips() if filter_func(x)]

    def unique_name(self):
        """ returns tag name that not exists """
        existing_tags = self.list()
        for i in range(0, 10000):
            tagname = "T" + ("0000" + str(i))[-4:]
            if tagname not in existing_tags:
                return tagname


class UniqidObject:
    """ Object having unique id, automatically created from suitable property """

    @property
    def uniqid(self):

        source_atrs = ['name', 'message']

        if not hasattr(self, "_uniqid"):
            # make uniqid. Should be unique "worldwide", so add some salt to distinguish.
            for value_name in source_atrs:
                if hasattr(self, value_name):
                    uniqid_base = getattr(self, value_name) + func.random_string(100)
                    setattr(self, '_uniqid', hashlib.md5(uniqid_base.encode('utf-8')).hexdigest())
                    break

            if not hasattr(self, "_uniqid"):
                raise AttributeError("Cannot create uniqid, missing one of attributes: %s" % ", ".join(source_atrs))

        return self._uniqid


class SmartTag(UniqidObject):
    """ We have smart tags here, right?
        emoticon is unicode string to display in front of tag.
        action is auto action provided on every whip with that tag asigned
        syntax is in commandline syntax, like "w 1 14"  (wait whip no 1 for 14 days)
        replacements are:
            $ .. whip md5
            % .. tag uniqid
        so for example above, the syntax is
        "w $ 14"
    """

    def __init__(self, name, action="", emoticon=0, desc=''):
        self.name = name
        self.emoticon = emoticon
        self.action = action
        self.desc = desc

    def action_do(self, context, whip):
        """ Performs SmartTag action on whip """
        modified = self.action.replace('$', whip.md5).replace('%', self.uniqid)
        command, args = commands.split_raw_command(modified)
        if not commands.perform_command(context, command, args):
            raise Exception("Unknown SmartTag action %s" % self.action)

#! /usr/bin/python3.4

# mocking files via this:
# https://github.com/otrabalhador/python-testing-by-examples/blob/master/docs/en/mocking/examples/reading-writing-on-files.md


import sys
import os
import unittest
from datetime import datetime, date, timedelta

# hack: very ugly: import from sibling directory, even if ran from parent dir (e.g. from ./test script)
sys.path.append(os.path.abspath('../src')) 
sys.path.append(os.path.abspath('src')) 

import pytest

import models, settings, whip
import test_whip


class MyTestCase(unittest.TestCase):

    def get_context(self):
        return models.ContextManager(whip.json_loader, whip.json_saver)


class Tmodels(MyTestCase):

    def test_0(self):
        context = self.get_context()

    def test_groups0(self):
        self.assertRegex(self.get_context().tags.unique_name(), r'[A-Z0-9]{2,6}')

    def test_whips_getter(self):
        c = self.get_context()
        # just suppose it returns something
        self.assertTrue(len(c.whips.really_all)>1)
        self.assertTrue(isinstance(c.whips.really_all[0],models.Whip))

    def test_transport(self):
        # should return string and sha256: dummy test if returns at least something
        data, hash = self.get_context().whips.undeleted[0].transport()
        self.assertGreater(len(data), 20)
        self.assertTrue("{" in data)
        self.assertTrue("}" in data)
        self.assertTrue(":" in data)
        
        self.assertEqual(len(hash), 64)

    def test_whips_get(self):
        c = self.get_context()
        self.assertIsNone(c.whips.get('tihs-is-not-a-md5-key'))

    def test_whip_priority(self):
        w = models.Whip()
        for p in range(0, 4):
            w.priority=p
        with self.assertRaises(ValueError):
            w.priority=5

    def test_whip_md5_nonexisting(self):
        w = models.Whip()
        self.assertIs(w.md5, None)

    def test_whip_raw(self):
        w = models.Whip()
        w.raw = 'bump'
        self.assertEqual(w.raw, 'bump')

    def test_whip_status(self):
        w = models.Whip()
        self.assertEqual(w.status, 'active')
        w.status = 'active'
        self.assertEqual(w.status, 'active')
        w.status = 'deleted'
        self.assertEqual(w.status, 'deleted')
        w.status = 'waiting'
        self.assertEqual(w.status, 'waiting')
        with self.assertRaises(ValueError):
            w.status = 'something-stupid'

    def test_whip_waiting(self):
        w = models.Whip()
        self.assertEqual(w.status, 'active')
        ts = datetime.timestamp(datetime.now())
        w.wait_until_timestamp(ts)
        self.assertEqual(w.status, 'waiting')

    def test_whip_nicedate(self):
        w = models.Whip()
        self.assertRegex(w.nicedate(), r'.{2,6}')
        w.timestamp = datetime.timestamp(datetime.now())
        self.assertRegex(w.nicedate(), r'Today')
        w.timestamp = datetime.timestamp(datetime.now() - timedelta(days=1))
        self.assertRegex(w.nicedate(), r'Ystdy')
        w.timestamp = datetime.timestamp(datetime.now() - timedelta(days=3))
        self.assertRegex(w.nicedate(), r'3 d')


def test_whip_tags():
    w = models.Whip()

    assert len(w.tags) == 0

    w.add_tag('AA')
    assert 'AA' in w.tags
    assert len(w.tags) == 1

    w.add_tag('AA')     # the same tag
    assert 'AA' in w.tags
    assert len(w.tags) == 1

    w.add_tag('BB')     # the same tag
    assert 'BB' in w.tags
    assert len(w.tags) == 2

    with pytest.raises(ValueError):
        w.add_tag('A')  # short name should not pass


def test_whip_init():
    # initialized Whip with no data should return some sane values
    w = models.Whip()
    assert w.message == ''
    assert w.priority == 0

    # md5 is set after message
    assert w.md5 is None
    w.message = "whatever"
    assert len(w.md5) > 10


def test_serialized():

    sync_data = { 'something': 'cool', 'number': 2 }

    w = models.Whip()
    w.message = 'Test'
    w.group = "BLAH"
    w.sync_data = sync_data

    ser = models.serialized(w)
    assert ser['group'] == 'BLAH'
    assert ser['message'] == 'Test'
    assert ser['sync_data'] == sync_data
    assert len(ser.keys()) in range(3, 50)  # smoke test if it is sane


def test_allowed_tagname():
    assert not models.allowed_tagname(None)
    assert not models.allowed_tagname('')
    assert not models.allowed_tagname('0')
    assert not models.allowed_tagname('-')
    assert not models.allowed_tagname('00')
    assert not models.allowed_tagname('A')
    assert models.allowed_tagname('AB')
    assert models.allowed_tagname('A0')
    assert models.allowed_tagname('AAAAAAA')
    assert not models.allowed_tagname('THISISTOOLONGTAGNAME' + ('A'*99))


def test_tags():
    c = test_whip.context_test()
    assert len(c.tags.list()) in range(3, 50)   # smoke test


def test_tag_nonempty():
    c = test_whip.context_test()
    assert len(c.tags.get('TEST')) == 2


if __name__ == "__main__":
    unittest.main()


def test_uniqidobject():

    # without "name" property, should raise arror
    class Dummy(models.UniqidObject):
        pass

    x = Dummy()
    with pytest.raises(AttributeError):
        _ = x.uniqid

    # with name property, should return sane uniqid
    class Dummy2(models.UniqidObject):

        def __init__(self):
            self.name = "whoa"

    # different between instances
    x = Dummy2()
    y = Dummy2()
    assert len(x.uniqid) > 10
    assert x.uniqid != y.uniqid

    # persist in same instance
    u1 = x.uniqid
    assert u1 == x.uniqid


def test_smarttag():

    x = models.SmartTag(name="bee")
    assert x.name == 'bee'
    assert len(x.uniqid) > 10


def test_get_whip_by_userspec():
    c = test_whip.context_test()
    w = c.whips.really_all[0]
    found = c.get_whip_by_userspec(w.md5)
    assert w.md5 == found


def test_modified_flag():
    c = test_whip.context_test()
    w = c.whips.really_all[0]

    with pytest.raises(AttributeError):
        x = w.modified

    w = c.whips.modify(w.md5, lambda x: True)
    assert w.modified is True

    c.save()
    with pytest.raises(AttributeError):
        x = w.modified



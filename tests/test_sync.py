#! /usr/bin/python3.4

import sys
import os

# hack: very ugly: import from sibling directory, even if ran from parent dir (e.g. from ./test script)
sys.path.append(os.path.abspath('../src')) 
sys.path.append(os.path.abspath('src')) 

import sync, models
import test_whip


def test_ping():
    assert sync.ping('noemail', 'nopassword', True) == False


def test_cachedputter():
    x = sync.CachedPutter()
    status, _ = x.put("wrong-url", "dummy", "dummy", models.Whip())
    assert status in (404, 504)


def test_cachedputter_get(monkeypatch):
    x = sync.CachedPutter()
    monkeypatch.setattr('sync.request', lambda a, b, c: (200, {}))
    whip = x.get("whatever", "dummy", "dummy")
    assert whip is None


def test_sync(monkeypatch):

    c = test_whip.context()
    monkeypatch.setattr('func.credentials_to_file', lambda x, y, z: None )
    monkeypatch.setattr('builtins.input', lambda x: 'seems_like_email@example.com')

    monkeypatch.setattr('func.credentials_from_file', lambda x: (None, None) )
    sync.sync(c)

    monkeypatch.setattr('func.credentials_from_file', lambda x: ('noemail', 'nopassword') )
    sync.sync(c)
    sync.sync(c, args=['c'])

    for status in [200, 500, 502, 404]:
        monkeypatch.setattr('sync.request', lambda x, y, z: (status, None))
        sync.sync(c)


def test_sync_up(monkeypatch):

    class MyEvent():
        """ Test stop event. For first couple of iterations returns False, than True"""
        
        def __init__(self):
            self.iterations = 0
    
        def is_set(self):
            self.iterations += 1
            return self.iterations > 12
            

    c = test_whip.context()
    monkeypatch.setattr('func.credentials_from_file', lambda x: ('noemail', 'nopassword') )
    monkeypatch.setattr('time.sleep', lambda x: None)
    sync.sync_up(c, None)

    monkeypatch.setattr('sync.request', lambda x, y, z: (200, {}))
    sync.sync_up(c, MyEvent())

"""
    Project - wide settings 
"""

import os
import sys

import func

NAME="Whip"

# format: X.XX (not X.X.X), with decimal point - for comparation purposes
VERSION="0.11"

# command that starts with with MAGIC_CHAR is treated as new whip
MAGIC_CHAR = "-"

# word starting with this is considered to be a tag
TAG_PREFIX = "#"

# Alias "group name" for manipulating whips without group
NO_GROUP_NAME = 'BLANK'

# how many lines on homepage scren occupy tag area
TAG_AREA_HEIGHT = 6

# colors
TAG_COLOR = 'lightblue'

# whip data files and folders 
REPOSITORY_ROOT = os.path.expanduser(func.arg('r', "~/.whip"))
FILENAME = os.path.join(REPOSITORY_ROOT, "whips.json")
BACKUP_FOLDERNAME = os.path.join(REPOSITORY_ROOT, 'backup')
CREDENTIALS_FILENAME = os.path.join(REPOSITORY_ROOT, "credentials")
SYNC_CACHE_FILENAME = os.path.join(REPOSITORY_ROOT, 'cache')
SYNC_SERVER_FILENAME = os.path.join(REPOSITORY_ROOT, "server")

if sys.platform == "linux":
    # HACK: Not great choice. Something from "/var/lib/whip" would be better, but those are read-only folders
    # and I dont want to mess up user while installing.
    CONFIG_FOLDER = os.path.expanduser("~")
elif sys.platform=="win32":
    CONFIG_FOLDER = "C:/ProgramData/Whip"
else:
    raise NotImplementedError ("""Don't know program_data folder for platform %s""" % sys.platform)

# make sure directories defined above exists
func.ensure_dir(REPOSITORY_ROOT)
func.ensure_dir(BACKUP_FOLDERNAME)
func.ensure_dir(CONFIG_FOLDER)

# Std sync server is stored in file, so user can modify it
# and thus have different setups choosen by -r parameter
SYNC_SERVER = func.value_stored_in_file(SYNC_SERVER_FILENAME, "https://whiphub.com/").strip()

# Machine ID is created once for lifetime of application on that host
MACHINE_ID = func.value_stored_in_file(os.path.join(CONFIG_FOLDER, ".whip-machineid"), func.random_string(100))

#! /usr/bin/python3.4

import sys
import os

# hack: very ugly: import from sibling directory, even if ran from parent dir (e.g. from ./test script)
sys.path.append(os.path.abspath('../src')) 
sys.path.append(os.path.abspath('src')) 

import importers


def test_gitlab():
    x = importers.GitlabImporter()
    assert x.interested("~/whatever") == False
    assert x.interested("https://gitlab.com/dimot9/team-management-system/") == True


def test_redmine():
    x = importers.RedmineImporter()
    assert x.interested("~/whatever") == False
    assert x.interested("https://redmine.pirati.cz/projects/resort-zivotni-prostredi") == True


def test_html():
    x = importers.HTMLImporter()
    assert x.interested("~/whatever") == False
    assert x.interested("https://example.com") == True


def test_file():
    x = importers.FileImporter()
    assert x.interested("~/.bashrc") == True
    assert x.interested("~/.nonexisting-file") == False
    assert x.interested("https://example.com") == False

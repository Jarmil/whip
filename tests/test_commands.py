#! /usr/bin/python3.4

import sys
import os
import io 
import pytest

# hack: very ugly: import from sibling directory, even if ran from parent dir (e.g. from ./test script)
sys.path.append(os.path.abspath('../src')) 
sys.path.append(os.path.abspath('src')) 

import commands
import test_whip


def test_homepage():
    commands.homepage(test_whip.context())

def test_list_with_priority():
    commands.list_whips_with_priority(test_whip.context())


def test_mylist(monkeypatch):
    monkeypatch.setattr('builtins.input', lambda x: '')
    c = test_whip.context()
    commands.mylist(c)
    commands.mylist(c, args=['a'])


def test_set_priority(monkeypatch):
    c = test_whip.context_test()
    with pytest.raises(ValueError):
        commands.set_priority(c, '1', 1)
        
    first_whip = c.whips.active[0]
    commands.set_priority(c, first_whip.md5, 2)
    first_whip = c.whips.active[0]
    assert first_whip.priority == 2


def test_rename_tag(monkeypatch):
    c = test_whip.context_test()

    first_whip = c.whips.active[0]
    first_whip.add_tag('BLEH')
    commands.rename_tag(c, first_whip.md5, 'BLEH', 'BLAH')
    first_whip = c.whips.active[0]
    assert 'BLAH' in first_whip.tags
    assert 'BLEH' not in first_whip.tags


def test_whip_detail():
    c = test_whip.context_test()
    commands.whip_detail(c, c.whips.active[0])


def test_wait():
    c = test_whip.context_test()

    commands.wait(c)
    commands.wait(c, args=['1'])
    commands.wait(c, args=['1', 'blahblah'])
    commands.wait(c, args=['1', '14'])


def test_add():
    c = test_whip.context_test()
    commands.add(c, '')
    commands.add(c, '- some whip')
    commands.add(c, '- some other whip !!!')
    commands.add(c, '- SOMEGROUP whip')
    commands.add(c, '- X whip')
    commands.add(c, '- SOMEGROUP')
    

def test_tag_list(monkeypatch):
    monkeypatch.setattr('builtins.input', lambda x: '')
    c = test_whip.context_test()
    commands.tag_list(c)


def test_remove_entry_by_hash():
    c = test_whip.context_test()

    count = len(c.whips.active)
    first_whip = c.whips.active[0]
    commands.remove_entry_by_hash(c, first_whip.md5)
    assert len(c.whips.active) == count - 1
    
    
def test_remove_entry(monkeypatch):
    c = test_whip.context_test()
    monkeypatch.setattr('builtins.input', lambda x: '')
    commands.remove_entry(c)
    commands.remove_entry(c, args=['x'])
    commands.remove_entry(c, args=['TEST'])

    monkeypatch.setattr('builtins.input', lambda x: 'y') # confirm delete group
    commands.remove_entry(c, args=['TEST'])


def test_list_tag_content():
    c = test_whip.context_test()
    commands.list_tag_content(c, 'TEST', True)
    commands.list_tag_content(c, 'TEST', False)
    commands.list_tag_content(c, None, False)
    commands.list_tag_content(c, -1, False)
    commands.list_tag_content(c, 'SOMETHING_DUMMY', False)


def test_myhelp(monkeypatch):
    monkeypatch.setattr('builtins.input', lambda x: '')
    commands.myhelp(test_whip.context_test())


def test_split_raw_command():
    com, args = commands.split_raw_command("ble   bla blo")
    assert com == "ble"
    assert len(args) == 2


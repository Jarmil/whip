"""
    Various functions
"""

import os
import re
import sys
import getopt
import random
import importlib
import shutil
from urllib.parse import urlparse

try:
    from termcolor import colored as _colored
except ModuleNotFoundError:
    _colored = None

import settings


def random_string(length):
    return "".join([ random.choice('abcdefghijklmnopqrstuvwxyz0123456789') for i in range(length)])


def terminal_size():
    """ Just wrap t std function to get sane defaults. Returns tuple or list (width, height) """
    return shutil.get_terminal_size((80, 20))


def arg(argument_name, default):
    ''' Returns value of command-line argument. 
        If argument type is flag, returns it as boolean
    '''
    try:
        foundargs, _ = getopt.getopt(sys.argv[1:], "r:")
    except getopt.GetoptError:
        foundargs = []
    for v in foundargs: 
        if v[0]=="-" + argument_name: 
            return v[1] if v[1] else True
    return default


def importer_list():
    """ Returns initialized clases of all imporers from module "importers" """
    module = importlib.import_module('importers')
    return [getattr(module, name)() for name in module.__all__]


def readfile(filename):
    """ Returns content of file as string
        If file not found, returns empty string
    """
    try:
        with open(filename, 'r') as f:
            return f.read()
    except FileNotFoundError:
        return ""


def ensure_dir(dirname):
    """ If directory does not exist, create it. dirname supports home directory char (~) """
    dirname = os.path.expanduser(dirname)
    if not os.path.isdir(dirname):
        os.mkdir(dirname)


def value_stored_in_file(filename, default):
    """ Return string value from filename. If there is no such fiel,
        create it and write default value to it. 
        Usage: To store configuration data 
    """
    if not readfile(filename):
        try:
            with open(filename, "w") as f:
                f.write(default)
        except:
            # Treat it as fatal. Stop program
            print("Cannot write to '%s'. Please check file permissions" % filename)
            exit()

    return readfile(filename)


def _credentials_canonical_url(url):
    parsed = urlparse(url)
    return "%s://%s/" % (parsed.scheme, parsed.netloc)


def credentials_from_file(url):
    """ Returns (as tuple username, password) saved credentials for server with given url,
        or (None, None) if not found
    """

    stored_url =  _credentials_canonical_url(url)
    try:
        with open(settings.CREDENTIALS_FILENAME, 'r') as f:
            for l in f:
                l = l.strip()
                if l.endswith('@' + stored_url):
                    return l[:-len(stored_url)-1].split(':')
    except FileNotFoundError:
        pass
    return None, None


def credentials_to_file(username, password, url):
    """ Saves credentials for given url to file"""

    stored_url = '@' + _credentials_canonical_url(url)

    with open(settings.CREDENTIALS_FILENAME, 'r') as f:
        lines = f.readlines()

    lines = [ x for x in lines if not x.endswith(stored_url + "\n") ]
    lines.append(username + ":" + password + stored_url + "\n")

    with open(settings.CREDENTIALS_FILENAME, 'w') as f:
        f.writelines(lines)


def colored(text, color, attrs=[]):
    """ Wrap to library functions termcolors.colored 
        Added support for more colors:
            lightblue
    """
    
    mycolors = {
        'lightblue': "\x1b[94m",
        'lightgray': "\x1b[37m",
        'darkgray': "\x1b[90m",
        'lightred': "\x1b[91m",
        'lightgreen': "\x1b[92m",
        'lightyellow': "\x1b[93m",
        'lightblue': "\x1b[94m",
        'lightmagenta': "\x1b[95m",
        'lightcyan': "\x1b[96m",
    }
    
    if color in mycolors:
        return mycolors[color] + str(text) + "\x1b[0m"
    elif _colored:
        if not color: color='white'
        return _colored(text, color, attrs=attrs)
    else:
        return(text)


def uncolored_length(text):
    """ returns lwngth of text stripped of coloring syntax """
    text = text.replace("\x1b[0m","") # color ends 
    initials = len(re.findall(r'\x1b\[..m', text)) # color starts
    return len(text) - 5 * initials

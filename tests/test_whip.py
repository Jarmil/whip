#! /usr/bin/python3.4

import sys
import os

# hack: very ugly: import from sibling directory, even if ran from parent dir (e.g. from ./test script)
sys.path.append(os.path.abspath('../src')) 
sys.path.append(os.path.abspath('src')) 

import whip, models, settings, func


def context():
    return models.ContextManager(whip.json_loader, whip.json_saver)


def context_test():

    def test_loader(context):
        w = models.Whip()
        w.message = 'Test 1'
        context.whips.add(w)
        w = models.Whip()
        w.message = 'Test 2'
        w.add_tag('TEST')
        context.whips.add(w)
        w = models.Whip()
        w.message = 'Test 3'
        w.add_tag('TEST')
        w.add_tag('TAG2')
        w.add_tag('TAG3')
        context.whips.add(w)

    def test_saver(context):
        pass

    return models.ContextManager(test_loader, test_saver)


def test_settings():
    assert settings.SYNC_SERVER=="https://whiphub.com/"


def test_split_whips():
    c = context()
    whip.split_whips(c, "")
    whip.split_whips(c, "1")
    whip.split_whips(c, "1, 2")


def test_version_number():
    whip.get_version_number()


def test_func():
    email, password = func.credentials_from_file('https://example.com')


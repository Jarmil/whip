
# Whip - command line TODO list with minimalistic approach

Written in python 3.6. Feel free to contribute.  
Main page: https://whiphub.com  
Author: Jarmil  
Feature requests, bug report: start issue here on gitlab  
More feedback: drop me a line at hi@jarmil.com . Be nice and concise ;-)  
License: [GNU Affero General Public License, version 3](https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License)

## Installation

    git clone https://gitlab.com/jarmil/whip.git
    cd whip
    ./install-venv

## Start

    cd <whip-directory>
    ./whip

## First run

Start Whip and type this commands to see what happens (ignore part after # hash)  
Press Enter after each command, naturally.  
Note: Commands are case sensitive

    - buy milk		# this adds your first whip (=todo item)
    - buy bread!		# another one, with priority
    - buy beer !!		# third whip, higher priority
    - #work call boss	# adds tag WORK to this whip
    - #work buy more beer #relax    # Define as much tags as you want
    WORK			# type tag name (without leading '#') to list whips with this tag
    [press Enter]		# displays 'homepage' - 10     most prioritized whips and group list
    l beer			# list whips containing this     keyword
    d 2			# delete second whip from last list
    [press Enter]		# homepage again
    h			# show help
    q			# quit

## Basic commands

Command list is not mantained in this doc. Type **h** in Whip to see actual command list.

## Tags

Tags are powerfull core feature of Whip. You can assign as many tags as you want to any Whip,
as shown above in chapter "First run". Main purpose of tags is sorting your whips.

  
Moreover, there are same predefined tags, called **Smart tags**. 
you can assign **SmartTags** to a whip the same way you assign 
your own tags. 

The main difference is, that assigning **SmartTag** to a Whip automatically performs an action, 
like setting the Whip to waiting state.
To see actual list of **SmartTags** and their acions, type:

    t 
   
## How to organize your work, using tags

Use tags extensivelly. For example, typing

    - #python #frontend #bug Items on frontpage are not alligned
    
assigns three Tags to a new Whip. Depending on context, you can later
list only bugs (when chatting with your client), 
python related tasks (when you want to hack) or frontend issues (while planing).
This kind of organization is extremelly flexible and fast to work with.    


## Command line arguments

-rVALUE  .. specify another whip repository root (instead of "~/.whip"). Example: -r~/.mywhip

## Import and synchronization with other systems

All this is made via **i** command. 
Usage: **i** source
where **source** can be filename or URL

- synchronizes (both way) issues (now just their  names) with Gitlab.
- imports issues from Redmine
- imports issues from any HTML page (`li` items)
- imports issues from local file (lines starting with dash **-**)

## Synchronization

To connect your whips to https://WhipHub.com, start Whip and enter **sync** command.

Choose your email and password. WhipHub.com will send  you an email message with activation link.  
When your account is activated, your whips are then synchonized with WhipHub.com

## Important files and directories

**~/.whip/**   all user configuration is stored here (unless changed by -r argument)
**~/.whip/whips.json**   your whips and some stuff, like configuration  
**~/.whip/backup/**  backups of your whips.json file. Filename is [timestamp].json  
**~/.whip/credentials**   stores your username and password for WhipHub.com as plain text  
**~/.whip/cache**         synchronization cache. If some sync problems occur, delete it. Command **sync c** does the same.

## Project structure

**src/**   source codes  
**test/**  unit tests  
**./test** runs tests and some code-quality controls  

"""
    Main module for running Whip in interactive mode
    
    Command line arguments:
        r  - specify another whip repository root (instead of "~/.whip")
    
"""

import os
from datetime import datetime
import shutil
import threading
import json
import readline     # silently enhances input()

import commands
import models
import settings
import sync


# contains newest version of client, obtained from server
available_client_version = 0


def split_whips(context, commandline):
    """ Scans commandline and tries to find whip ids. Recognizes syntax:
            7 blalbla
        returns: ( [md5, md5, md5], rest_of_command)
        if no whips found, returns ([],'')
    """
    command = commandline.split(' ')[0].strip()
    rest = commandline[len(command):].strip()
    md5 = context.get_whip_by_userspec(command)

    if md5:
        return [md5], rest
    elif models.allowed_tagname(command):
        return list(map(lambda x: x.md5, context.tags.get(command))), rest.strip()
            
    return [], ''


def _delete_old_backups(folder):
    """ leave only 20 newest backup files in folder """
    dir_content = [os.path.join(folder,f) for f in os.listdir(folder)]
    files = [f for f in dir_content if os.path.isfile(f)]
    files = sorted(files, key=os.path.getmtime)[:-20]
    for f in files:
        os.remove(f)


def get_version_number():
    """ ask server for newest client version. save it to global variable (pretty ugly) """
    global available_client_version
    _, data = sync.ping2('', '')
    if data:
        available_client_version = data['available_client_version']


def json_loader(context):
    """ Abstracted loader: loads saved data from json file and stores it into 
        context structure
    """
    try:
        with open(settings.FILENAME, "r") as f:
            content = f.read()
    except FileNotFoundError:
        return None

    try:
        data = json.loads(content)
    except:
        # data seems corrupted - fail TODO silently?
        pass
        
    try:
        entries = data['entries']
    except KeyError:
        return
            
    for key in entries:
        for x in entries[key]:
            wh = models.Whip(data=x, md5=key)
            context.whips.add(wh)


def json_saver(context):
    """ Abstracted saver: saves whips to json file from context 
    """
    mydata = {}
    
    # whips
    try:
        mydata['entries'] = context.whips.data
    except AttributeError:
        return

    with open(settings.FILENAME, "w") as f:
        f.write(json.dumps(mydata, indent=4))


if __name__ == '__main__':

    # ask server for available client version (in separate thread to prevent delay on start)
    threading.Thread(target=get_version_number, name="temp").start()

    # backup data; backup files are named <timestamp>.json
    filename = os.path.join(settings.BACKUP_FOLDERNAME, "%s.json" % datetime.timestamp(datetime.now()))
    try:
        shutil.copyfile(settings.FILENAME, filename)
    except FileNotFoundError:
        pass

    # ensure backup folder will not grow too much
    _delete_old_backups(settings.BACKUP_FOLDERNAME)

    # load or create program state, including whips
    context = models.ContextManager(json_loader, json_saver)

    commands.homepage(context, available_client_version)

    while True:

        # ensure exactly 1 sync thread is running
        if not len([x for x in threading.enumerate() if x.name=='up']):
            event_stop = threading.Event()
            threading.Thread(target=sync.sync_up, name="up", args=(context,event_stop)).start()
            
        # check whips in waiting state, if they're still waiting
        if context.whips.check_waiting():
            context.save()

        commandline = input("{name}> ".format(name=settings.NAME.lower())).strip()
        command, args = commands.split_raw_command(commandline)
        split_ids, rest_of_command = split_whips(context, commandline)

        # special command to list all whips in a tag, including waiting whips
        if command == '*' and context.guessed_tagname:
            list_entire_tag = True
        else:
            list_entire_tag = False
            context.guessed_tagname = context.tags.guess(command)

        if command == 'q':
            event_stop.set()
            break

        elif context.guessed_tagname and (not args) and (command not in commands.mapping.keys()):
            commands.list_tag_content(context, context.guessed_tagname.name, list_all=list_entire_tag)

        elif command.startswith(settings.MAGIC_CHAR):
            # add whip
            commands.add(context, commandline)
            if len(context.whips.active) <= 10:
                commands.homepage(context, available_client_version)

        elif split_ids:

            # Experimental: command syntax <id> <command> <args>
            args = rest_of_command.split(' ')
            if len(args) == 1 and args[0] in ('!', '!!', '!!!'):
                for id in split_ids:
                    commands.set_priority(context, id, len(args[0]))

            elif len(args) == 1 and args[0] == '.':
                for id in split_ids:
                    commands.set_priority(context, id, 0)

            elif len(args) == 1 and models.allowed_tagname(args[0]):
                for id in split_ids:
                    commands.rename_tag(context, id, command, args[0])
                print("Tag %s renamed to %s" % (command, args[0]))
                commands.mylist(context, 10)

            elif len(args) == 1 and (args[0][:2]=="-#") and models.allowed_tagname(args[0][2:]):

                def deleter(whip):
                    whip.delete_tag(args[0][2:])

                # Remove tag from whip
                for whip_id in split_ids:
                    context.whips.modify(whip_id, deleter)

                print('Tag %s removed' % args[0][2:])
                commands.mylist(context, 10)

            elif rest_of_command:
                # modify whip, but only if it wasn't defined by tag name
                # (othervise it would modify first whip with that tag, which is confusing)
                # HACK: add function expects command line startting with magic char, so add it
                if command in context.tags.list():
                    print("""Not sure what you want to do with '%s'.
If you wish to add whip with this tag, use %s %s something""" % (command, settings.MAGIC_CHAR, command))
                else:
                    commands.add(context, settings.MAGIC_CHAR+rest_of_command, whip_md5=split_ids[0])

            else:
                # just show first whip
                commands.whip_detail(context, context.whips.get(split_ids[0]))

        elif commands.perform_command(context, command, args):
            # ordinary commands
            pass

        elif commandline == '':
            context.reversed_homepage = not context.reversed_homepage
            commands.homepage(context, available_client_version)

        else:
            print('%s: command not found' % command)

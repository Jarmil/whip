""" Function for sync whips with cloud server """

import re
import urllib.parse
import requests
import json
import time
import sys

import settings
import models
import func

def request(url, username, password):
    """ Makes authorized request to server, returns tuple (status, JSON data or None)
        status is HTTP code returned by server or 504 for any connection error
        (this function acts as proxy, so returning this code is __fine__)
    """
    myurl = urllib.parse.urljoin(settings.SYNC_SERVER, url)
    
    try:
        response = requests.get(myurl, auth=(username, password),
            headers={
                'User-Agent': "Whip/%s (%s)" % (settings.VERSION, sys.platform),
                'Machine-ID': settings.MACHINE_ID
                })
    except requests.exceptions.ConnectionError:
        return 504, None
       
    try:
        js = response.json()
    except json.decoder.JSONDecodeError:
        js = None

    return response.status_code, js


def ping2(email, password):
    return request("/api/v1/ping/", email, password)


def ping(email, password, verbose=True):
    """ ping server - just test if is authenticated. 
        Write messages and result success
    """
    
    def _print(message):
        if verbose:
            print(message)
            
    def _get_message(data):
        try:
            return data['message']
        except (KeyError, TypeError):
            return '-nothing-'
    
    _print("""Connecting {email} to {server} ...""".format(email=email, server=settings.SYNC_SERVER))
    status, data = ping2(email, password)

    if status==200:
        # everyting is fine, save credentials if not already saved
        _print("Login succesfull, server returned: %s" % _get_message(data))
        return True
    else:
        _print("""Failed ({code}). Server responded: {message}""".format(code=status, 
            message=_get_message(data)))
        return False


class CachedPutter():
    """ Cached communication with sync server """
    
    def __init__(self):
        self.filename = settings.SYNC_CACHE_FILENAME
        try:
            with open(self.filename, 'r') as f:
                self.__cache = [x.strip() for x in f.readlines()]
        except FileNotFoundError:
            self.__cache = []

    def _save(self):
        with open(self.filename, 'w') as f:
            f.writelines([x+"\n" for x in self.__cache])

    def clear(self):
        """ clear cache """
        self.__cache = []
        self._save()

    def cache_add(self, hash):
        """ add one hash to cache, save it """
        self.__cache.append(hash)
        self.__cache = list(set(self.__cache))
        self._save()

    def get(self, hash, username, password):
        """ Makes cached authorized request to server, returns whip identified by hash
            If it was in cahe, return None
            , if it wasnt downloaded before
        """

        # prevent downloading the same data again
        if hash in self.__cache:
            return None
            
        status, data = request("/api/v1/whip/%s/" % hash, username, password)
        if status == 200:
            try:
                whip = models.Whip(data=data['data'], md5=data['data']['md5'])
            except KeyError:
                return None
            self.cache_add(hash)
            time.sleep(0.2)
            return whip

    def put(self, url, username, password, whip):
        """ Makes authorized request to server, sends up whip data if it wasn't sent before.
            Returns tuple (status, JSON data or None)
            status is HTTP code returned by server or 504 for any connection error
            (this function acts as proxy, so returning this code is __fine__)
        """

        # prevent sending the same data again
        payload, payload_hash = whip.transport()
        if payload_hash in self.__cache:
            return 200, None

        myurl = urllib.parse.urljoin(settings.SYNC_SERVER, url)
        try:
            header = {
                "Content-Type" : "application/json",
            }
            response = requests.put(myurl, auth=(username, password), headers = header, data=payload)
        except requests.exceptions.ConnectionError:
            return 504, None
           
        try:
            js = response.json()
        except json.decoder.JSONDecodeError:
            js = None
            
        # if success, store md5 hash of sent data, so next time we will not send it again
        if response.status_code==200:
            self.cache_add(payload_hash)
            time.sleep(0.2)

        return response.status_code, js


def sync(context, args=[]):

    def _input(prompt, regexp):
        while True:
            val = input(prompt).strip()
            if re.match(regexp, val) or val=='':
                break
            else:
                print("Invalid")
        return val

    if args and args[0]=='c':  # command to clear cache
        putter = CachedPutter()
        putter.clear()
        print("Cache cleared")
        return

    email, password = func.credentials_from_file(settings.SYNC_SERVER)

    if email and password:
        status, data = ping2(email, password)
        if status==200:
            # ideal case: user authenticated from saved credentials
            print("Connection to %s is OK" % settings.SYNC_SERVER)
            return 
        elif status in range(500, 600):
            print("""Problem on server side (server returned %s). Try again later.""" % status)
            return
        else:
            print("""Please enter new email and password. [Enter] to skip:""")
    else:
        print("""Synchronization server %s requires email and password:""" % settings.SYNC_SERVER)
        
    email = _input('Email:', r'[^@]+@[^@]+\.[^@]+')
    password = _input('Password (6 characters at least):', r'.{6,999}') if email else ''

    if email and password and ping(email, password):
        func.credentials_to_file(email, password, settings.SYNC_SERVER)
        return

    return print("Not synced\n")


def sync_up(context, event_stop):
    """ event_stop ... communication event, when is set, this thread stops as soon as """

    email, password = func.credentials_from_file(settings.SYNC_SERVER)
    sleep_length = 10   # in tenths of second
    iteration = 0
    known_lmd = None    # last data modification date on server
    
    # it not connected, stop thread. Next time will be started when user enters (any} command;
    # that seems to be a reasonable interval to check again living connection
    if not ping(email, password, verbose=False):
        return

    while True:
    
        for x in range(sleep_length): # for purposes of granularity and fast stop
            time.sleep(0.1)
            if event_stop.is_set():
                return
        sleep_length = 10
        
        putter = CachedPutter()
        
        # sync up
        for whip in context.whips.really_all:
            if event_stop.is_set():
                return
            code, message = putter.put( "/api/v1/whip/", email, password, whip)

            # Unauthorized or server error? Stop sync and make retry interval way longer to prevent spam
            if code in range(400, 600):
                sleep_length = 600
                break

        # sync down - every 30th iteration to prevent spam
        if (iteration % 30) == 0:
            
            _, data = ping2(email, password)
            try: 
                lmd = data['last_data_modification']
            except (KeyError, TypeError):
                lmd = None
            
            if lmd and lmd != known_lmd:
                known_lmd = lmd
                # TODO this is hotfix. Not ideal: Should be less demanding for the server. 
                status, data = request("/api/v1/list/full/", email, password)
                if data:
                    for hash in data['data']:
                        if event_stop.is_set():
                            return
                        whip = putter.get(hash, email, password)
                        if whip:
                            context.whips.add(whip)
                            context.save()

        iteration += 1

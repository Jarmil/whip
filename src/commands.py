"""
    Commands invoked by whip.py
"""

import math
from datetime import datetime, timedelta
import re
import os
import shutil

import settings
import func
import sync


def myhelp(context, args=[]):

    table = ""
    for x in mapping:
        table += "%s: %s\n" % (('    ' + x)[-5:], mapping[x][1])
    help_text = """Terminology: whip = one item on TODO list.
 To add whip, simpy write line starting with {magic_char}
 Example: - Make love with Sarah, tonight

Magic included: 
 - one to three !s at whip end sets priority. Ex: - buy flowers for Sarah!!
 - anything starting with # defines tag.  Ex: - #home clean up mess #asap
 - Auto highlighting today's appointments: If your whip contains a date, 
   e.g. "03-24" matching current date, such whip is shown first and highlighed

Show whip or tags:
  2           show details of 2nd whip
  LOVE        list whips with tag LOVE
              (you can type just part of the name if not ambiquious, like 'LO')

Basic whip manipulation:
  2 #URGENT   add tag URGENT to 2nd whip
  2 -#URGENT  remove tag URGENT from 2nd whip
  2 LOVE      (shortcut alias) add tag LOVE to 2nd whip
  LOVE SARAH  rename tag LOVE to SARAH
  2 !         set 2nd whips priority (2 !! or 2 !!! works too)
  2 .         unset priority
  2 say bye   change text of 2nd whip to 'say bye'

Commands:
{table}

  """.format(
    name=settings.NAME, table=table, magic_char=settings.MAGIC_CHAR).split("\n")

    print_paged(help_text)
    input("[Enter to continue]")
    homepage(context)


def clear_screen():
    os.system('cls' if os.name == 'nt' else 'clear')


def print_paged(text, extraspace=0):
    """ Prints paged text, which is in form [line1, line2,...]
        extraspace is number of lines to keep blank at bottom of the page
    """
    terminal_lines = func.terminal_size()[1] - 1 - extraspace
    while text:
        # clear_screen()
        print("\n".join(text[ :terminal_lines]))
        text = text[terminal_lines:]
        if text:
            input("[Enter to continue]")


def homepage(context, available_version=0):

    clear_screen()
    
    email, _ = func.credentials_from_file(settings.SYNC_SERVER)
    if email:
        syncinfo =  "Connected to %s" % settings.SYNC_SERVER
    else:
        syncinfo = "Not synced. Type %s to connect to %s" % (func.colored('sync', 'yellow'), settings.SYNC_SERVER)
    
    if float(available_version) > float(settings.VERSION):
        verinfo = func.colored("Client version %s available - git pull" % available_version, attrs=['reverse'])
    else:
        verinfo = ""

    header = """{name} {version}: Minimalistic command-line todo list.""".format(
          name=settings.NAME, version=settings.VERSION)
    if context.reversed_homepage:
        header += " (OLDEST FIRST)"
        coloring_attrs = ['reverse']
    else:
        coloring_attrs = []
    header = (header + " "*100)[:func.terminal_size()[0]]
    print(func.colored(header, '', attrs=coloring_attrs))

    print("""{syncinfo}  {verinfo}
Write command (type h for help) or add whip (todo item) starting with {magic_char}
""".format(name=settings.NAME, version=settings.VERSION, magic_char=settings.MAGIC_CHAR,
        syncinfo=syncinfo, verinfo=verinfo))

    if context.whips.active:
        mylist(context, 10, reversed_order=context.reversed_homepage)
        print()
        short_tag_list(context)
    else:
        print("""Now type this to add your first whip: - call Sarah [Enter]
""")


def set_priority(context, md5, priority):
    """ Set whips priority """
    def modifier(w):
        w.priority = priority
        
    context.whips.modify(md5, modifier)
    context.save()
    print("Whip %s priority set" % (md5[:7]))


def rename_tag(context, md5, old_tag, new_tag):
    """ Rename tag in whip"""
    def modifier(w):
        w.delete_tag(old_tag)
        w.add_tag(new_tag)

    context.whips.modify(md5, modifier)
    context.save()


def wait(context, args=[]):
    """ Set whip to 'waiting' status. As every modification of whip, it is made as adding modified whip clone
        If no delay is set, defaults to 1 day
    """
    
    def _usage(prefix):
        print("""%s
 Examples: 
 w 1 14       # 1st whip will be in waiting state for 14 days
 w HOME 14    # all whip with tag HOME will be in waiting state for 14 days
 """ % prefix)
    
    def modifier(w):
        ts = datetime.timestamp(datetime.now()+timedelta(days=delay_days))
        w.wait_until_timestamp(ts)

    if args:

        if len(args) < 2:
            delay_days = 1
        else:
            try:
                delay_days = int(args[1].strip())
            except ValueError:
                return _usage("Delay must be number in days.")

        cleaned = args[0].strip()

        if cleaned in context.tags.list():
            # Set wait for all whips with this tag. Cowardly ask.
            whip_with_tag = context.tags.get(cleaned)
            if input("Do you want to wait all {count} whips with tag {name} (y/N)?".format(
                    name=cleaned, count=len(whip_with_tag))) == 'y':
                for whip in whip_with_tag:
                    context.whips.modify(whip.md5, modifier)

        else:
            hash = context.get_whip_by_userspec(args[0].strip())
            if hash:
                context.whips.modify(hash, modifier)
                print("Whip %s will wait for %s days." % (hash[:7], delay_days))
            else:
                print("""Whip '%s' not found """ % args[0].strip())

        context.save()

    else:
        l = context.whips.waiting
        list_whips(context, l, final_line=False)
        print("Shown %s waiting whip(s)" % len(l))


def add(context, commandline, whip_md5=None):
    """ adds entry from commandline, including leading character
        if whip_md5 is specified, modifies this whip instead
    """
    raw_message = commandline[1:].strip()

    def modifier(w):

        nonlocal raw_message

        # Whip ending with ! defines priority
        priority = 0
        while priority < 3 and raw_message.endswith('!'):
            priority += 1
            raw_message = raw_message[:-1].strip()

        # find tags, add 'em to whip and srip'em from message
        my_regex = r'#([a-zA-Z0-9\-_]{2,99})'
        while True:
            m = re.search(my_regex, raw_message)
            if m:
                w.add_tag(m.group(1).upper())
                raw_message = re.sub(my_regex, '', raw_message, count=1)
            else:
                break

        raw_message = raw_message.strip()

        # Ugly HACK: prevent adding nonsenses
        if raw_message in ['-', '#', '-#']:
            print("Dont know what to do with '%s'" % raw_message)
            return

        # avoid adding or replacing empty messages
        if raw_message:
            w.message = raw_message
        else:
            if not whip_md5:
                print("Error: Cannot add whip, there is no message")
                return

        # avoid setting priority from something to zero
        if w.priority == 0 or priority:
            w.priority = priority

        try:
            autosync_source = w.sync_data['source']
        except KeyError:
            autosync_source = ""

        if w.sync_data and not w.autosync:
            answer = input("""Would you like to auto sync this whip with '%s' [y/N]?""" % autosync_source)
            w.autosync = "yes" if (answer == 'y') else "no"

        if w.autosync:
            print("Syncing with %s..." % autosync_source)
            for importer in func.importer_list():
                if importer.sync_up(w):
                    break
            else:
                print("Error: don't know how to sync up this whip.")

    if raw_message:
        context.whips.modify(whip_md5, modifier)
        context.save()


def short_tag_list(context):
    """ Compressed tag list for homepage """

    found_tags = [[x, len(context.tags.get(x.name))] for x in context.tags.list()]
    if not found_tags:
        return

    found_tags.sort(reverse=True, key=lambda x: x[1] + 9999 * len(x[0].action))  # tags with action first, than by usage

    max_name_length = max([len(x[0].name) for x in found_tags])
    available_columns = math.floor(80 / (max_name_length+9))

    strings = [""" {emo}{tag}  {count}""".format(
        emo=smart_tag.emoticon or "  ",
        tag=func.colored((smart_tag.name+' '*99)[:max_name_length], settings.TAG_COLOR),
        count=func.colored((str(count)+' '*3)[:3], settings.TAG_COLOR)
    ) for smart_tag, count in found_tags]

    # Too many tas would push out homepage header, so limit it to settings.TAG_AREA_HEIGHT
    visible_strings = (settings.TAG_AREA_HEIGHT-1) * available_columns
    overflow = len(strings) - visible_strings
    if overflow > 0:
        strings = strings[:visible_strings-1]
        strings.append(
            func.colored("""   +%s (type """ % (overflow+1), settings.TAG_COLOR) +
            func.colored('t', 'lightyellow') +
            func.colored(')', settings.TAG_COLOR))

    print("""Tags (to list whips with tag, type tag name. Ex: %s)""" % func.colored(found_tags[-1][0].name, "lightyellow"))
    while strings:
        line = "|".join(strings[:available_columns])
        strings = strings[available_columns:]
        print(line)
    print()


def tag_list(context, args=[]):
    """ List tags and counts """

    found_tags = [[x, len(context.tags.get(x.name))] for x in context.tags.list()]
    if not found_tags:
        return

    max_name_length = max([len(x[0].name) for x in found_tags])

    strings = [""" {emo}{tag}  {count}  {command}  {desc}""".format(
        emo=smart_tag.emoticon or " ",
        tag=func.colored((smart_tag.name+' '*99)[:max_name_length], settings.TAG_COLOR),
        count=func.colored((str(count)+' '*3)[:3], settings.TAG_COLOR),
        command=smart_tag.action,
        desc=smart_tag.desc
    ) for smart_tag, count in found_tags]

    print_paged(strings)


def format_whip_for_print(context, whip, order):

    def _ellipse(msg, length):
        if len(msg) > length:
            return msg[:length-1] + ">"
        else:
            return msg

    if context.verbose:
        tag_string = " ".join(whip.tags)
    else:
        tag_string = ("  %s#" % len(whip.tags) if len(whip.tags) else "   ")[-3:]

    prefix = """{id} {mydate} {tag} """.format(
        id=func.colored(("       %s"%order)[-2:], 'yellow'),
        mydate=func.colored(whip.nicedate(), 'green'),
        tag=func.colored(tag_string, 'lightblue')
        )
    
    if whip.status == 'waiting':
        prefix += func.colored("Wait %s " % whip.nice_wait_condition, 'lightyellow')

    if whip.public_url:
        prefix += func.colored('P ', 'lightyellow')

    if whip.priority:
        prefix += func.colored("(%s) " % ('!'*whip.priority), 'red')

    message = _ellipse(whip.message, 99999 if context.verbose else 80 - func.uncolored_length(prefix))
    if whip.is_today:
        message = func.colored(message, '', attrs=['reverse'])
    return """{prefix}{message}""".format(prefix=prefix, message=message)


def list_whips(context, whips, final_line=True):
    """ Print whips list """
    i, last_view_mapping = 0, {}
    whiplist = []
    if len(whips):
        for x in whips:
            i += 1
            # save last view indexes for later use (e.g. in delete function)
            last_view_mapping[str(i)] = x.md5
            whiplist.append(format_whip_for_print(context, x, i))
    print_paged(whiplist, 1)

    if final_line:
        if i:
            print("""Shown %s of %s active (%s waiting)""" % 
                (i, len(context.whips.active), len(context.whips.waiting)))
        else:
            print("""[empty]""")
            
    context.set_last_view_mapping(last_view_mapping)


def whip_detail(context, whip):
    """ Print detailed info of a whip """

    screen_width = shutil.get_terminal_size((80, 20))[0]

    print("""{header}
{mydate} {priority} {waiting}
Tags: {tags}
{delimiter}
{publish_info}
{message}
    """.format(
        header=('-' * int((screen_width-len(whip.md5)-2)/2) + ' ' + whip.md5 + ' ' + '-' * 999)[:screen_width],
        delimiter='-' * screen_width,
        mydate=func.colored(whip.nicedate(), 'green'),
        priority=func.colored(("(%s) " % ('!' * whip.priority)) if whip.priority else '', 'red'),
        waiting=func.colored(("Wait %s " % whip.nice_wait_condition) if whip.status=='waiting' else '', 'lightyellow'),
        md5=whip.md5,
        tags=func.colored(" ".join(whip.tags), 'lightblue'),
        publish_info=func.colored(("Published at %s" % whip.public_url) if whip.public_url else "", 'lightyellow'),
        message=whip.message,
    ))


def mylist(context, count=999999999, args=[], reversed_order=False):
    """ Lists last count entries or filtered entries
        args[0] is search (filter) string 
    """
    l = context.whips.active
    searchstring = args[0].lower() if args else ''
    
    if searchstring:
        l = [x for x in l if searchstring in x.message.lower()]

    if reversed_order:
        l.reverse()

    if not searchstring:
        l = l[:count]

    list_whips(context, l)


def remove_entry_by_hash(context, hash):
    context.whips.delete(hash)
    context.save()
    print("""Whip %s deleted""" % hash[:7])
    mylist(context, 10)


def remove_entry(context, args=[]):
    """ Remove last entry. 
        args[0] is id or hash of entry to delete
    """
    if args:
        cleaned = args[0].strip()
        
        if cleaned in context.tags.list():

            # Delete all whips with this tag. Cowardly ask.
            whip_with_tags = context.tags.get(cleaned)
            if input("Do you want to delete all {count} whips with tag {name} (y/N)?".format(
              name=cleaned, count=len(whip_with_tags))) == 'y':
                for whip in whip_with_tags:
                    remove_entry_by_hash(context, whip.md5)

        else:

            hash = context.get_whip_by_userspec(cleaned)
            if hash:
                remove_entry_by_hash(context, hash)
            else:
                print("""Whip '%s' not found """ % cleaned)
                return

    else:

        # remove last
        remove_entry_by_hash(context, context.whips.active[0].md5)


def list_whips_with_priority(context, args=[]):
    w = [x for x in context.whips.active if x.priority>0]
    list_whips(context, w)


def file_import(context, args=[]):

    def _print_whip(w):
        print("""Found: {line}""".format(line=w.message))

    source = args[0].strip() if args else ""

    if not source:
        print("""Specify file to import""")
        return 

    for importer in func.importer_list():
        if importer.interested(source):
            rawlist = importer.get_whips(source)

            if rawlist:
                for whip in rawlist:
                    _print_whip(whip)

                tagname = context.tags.unique_name()

                print("""
Note: Imported whips will have tag {tag} set.
Import {count} lines?

 i - interactive import, confirm every new whip
 a - import all
 n - do nothing""".format(tag=func.colored(tagname, "lightyellow"), count=len(rawlist)))
                command = input("Select action (i/a/N): ")

                if command in ('i', 'a'):
                    i = 0
                    for whip in rawlist:
                        i += 1
                        if command == 'a':
                            doit =  True
                        else:
                            _print_whip(whip)
                            command2 = input("Import (%s of %s)? (y/N/q): " % (i, len(rawlist)))
                            if command2 == 'q':
                                return
                            doit = (command2 == 'y')
                        if doit:
                            whip.add_tag(tagname)
                            context.whips.add(whip)

                    context.save()

                homepage(context)
                return

            else:
                print(importer.excuse())
                return

    print("""Not found: %s""" % source)


def verbose(context, args=[]):
    """ Switch program verbosity on/off"""
    context.verbose = not context.verbose
    print("Verbose mode %s" % ("on" if context.verbose else "off"))


def list_tag_content(context, tag_name, list_all=False):
    """ list whips with tag_name """
    guessed_tag = context.tags.get(tag_name)
    active = [x for x in guessed_tag if x.status == 'active']
    waiting = [x for x in guessed_tag if x.status == 'waiting']
    if list_all:
        list_whips(context, guessed_tag, final_line=False)
    else:
        list_whips(context, active, final_line=False)
    print(func.colored(tag_name, "lightblue") +
          """: %s active""" % len(active) +
          ((""", %s waiting""" % len(waiting)) if len(waiting) else "") +
          ((""" (type %s to see waiting whips as well)""" % func.colored('*', 'lightyellow')) if len(waiting) and not list_all else ""))


def publish(context, args=[]):
    """ Publishes whip."""

    def _usage(prefix):
        print("""%s
 Examples: 
 p 1           # Publish 1st whip at https://whiphub.com/youremail/
 p 1 UrlSUFFIX # Publish 1st whip at https://whiphub.com/youremail/urlsuffix/
               # Url suffix can contain letters only, will be lowercased 
 """ % prefix)

    if not args:
        return _usage('Specify Whip to publish')

    if len(args) == 1:
        slug = "*"
    elif len(args) == 2:
        if re.match(r'[a-zA-Z]+', args[1]):
            slug = args[1].lower()
        else:
            return _usage("The URL suffix can contain letters only.")
    elif len(args) > 2:
        return _usage("Too many arguments")

    def modifier(w):
        w.publish_slug = slug

    whip_hash = context.get_whip_by_userspec(args[0].strip())
    if whip_hash:
        w = context.whips.modify(whip_hash, modifier)
        context.save()
        print("Whip is published under %s" % w.public_url)
    else:
        print("""Whip '%s' not found """ % args[0].strip())


def unpublish(context, args=[]):
    """ Unpublishes whip."""

    def _usage(prefix):
        print("""%s
 Examples: 
 u 1           # Unpublishes 1st whip
 """ % prefix)

    if not args:
        return _usage('Specify Whip to unpublish')

    def modifier(w):
        w.publish_slug = ""

    whip_hash = context.get_whip_by_userspec(args[0].strip())
    if whip_hash:
        context.whips.modify(whip_hash, modifier)
        context.save()
        print("Whip was unpublished")
    else:
        print("""Whip '%s' not found """ % args[0].strip())


# Map commands to functions and help strings here
mapping = {
    'q'    : (None, """Quit"""),
    'h'    : (myhelp, """Display help"""),
    'l'    : (mylist, """List whips. To filter whips about Sarah, type "l sarah" """),
    'd'    : (remove_entry, """Delete last whip. To remove third, type "d 3". """),
    't'    : (tag_list, """List tags"""),
    'v'    : (verbose, """Switch verbose mode on/off"""),
    # 'p'    : (publish, """Publishes Whip. Type "p" to get help"""),
    # 'u'    : (unpublish, """Unpublishes Whip. Ex: "u 1" """),
    'i'    : (file_import, """Import todo list from file or online source:
      - file: i ~/my-todo-list 
      - redmine agenda: i https://redmine-hosting.org/projects/my-project
      - gitlab open issues: i https://gitlab.com/Jarmil/whip
      - any URL: i https://example.com/my-todo-list.html"""),
    '!'    : (list_whips_with_priority, """Show whips with priority"""),
    'w'    : (wait, """Sets whip to wait state. Ex: "w 1 14" waits 14 days. 
       Default is 1 day: "w 3" will wait 3rd whip for 1 day"""),
    'sync' : (sync.sync, """Sync whips with cloud server. "sync c" - clears cache (resync)"""),
}


def split_raw_command(raw_commandline):
    command = raw_commandline.split(' ')[0].strip()
    args = [x for x in raw_commandline.split(' ')[1:] if x]
    return command, args


def perform_command(context, command, args):
    """ Tryies to find and perform cmmand from command list.
        Returns, if command was performed
    """
    if command in mapping.keys():
        mapping[command][0](context, args=args)
        return True
    else:
        return False

"""
    Importers for various 3rd party todo lists, e.g. file list or URL

    for new importer:
        - subclass Importer abstract class
        - implement necessary methods
        - add this class to __all__ field
    
    Importers are called in order defined in __all__
"""

import os
import re
import abc
import urllib
import requests
import json
from bs4 import BeautifulSoup

import settings
import func
import models

__all__ = (
    'FileImporter', 
    'RedmineImporter', 
    'GitlabImporter', 
    'HTMLImporter', 
  )


class Importer(abc.ABC):
    """ abstract  class as a common ancestor for all importers """

    @abc.abstractmethod
    def interested(self, source):
        """ Subclass should return boolean, if it can import data from that source 
            source .. .stripped string entered by user, e.g. "~/somefile"
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_whips(self, source, ask_for_credentials=True):
        """ Subclass should return list of whips imported from source.
            if ask_for_credentials, importer will (in case of unauthorized) 
                interact with user, try to obtain credentials and save it in credentials file
            Tag will be overwriten by caller
        """
        raise NotImplementedError

    @abc.abstractmethod
    def excuse(self):
        """ If get_lines() retuirned nothing, this property returns a nice text with reason, 
            what happened, eg. bad credentials, file empty....
        """
        raise NotImplementedError

    @abc.abstractmethod
    def sync_up(self, whip):
        """ Auto synces whip to its source, using data from whip.sync_data.
            Return boolean as success. 
            (If importer don't know how what to do with this whip (e.g. FileImporter with GitLab issue),
            returns False
        """
        raise NotImplementedError



class FileImporter(Importer):

    def _filename(self, source):
        return os.path.expanduser(source) # replace ~

    def interested(self, source):
        """ Is interested in everything what looks like local file"""
        
        filename = self._filename(source)
        return os.path.isfile(filename)

    def get_whips(self, source, ask_for_credentials=True):
        filename = self._filename(source)
        
        with open(filename, 'r') as f:
            self.lines = f.readlines()
            
        to_import = []
        for line in self.lines:
            line = line.strip()
            if re.match(r'-.+', line):
                myline = line[1:].strip()
                whip = models.Whip()
                whip.message = myline
                to_import.append(whip)
        return to_import

    def sync_up(self, whip):
        return False

    def excuse(self):
        return """Found no lines to import (starting with {magic}). Examined {lines} line(s) total.""".format(
            magic=settings.MAGIC_CHAR,
            lines=len(self.lines)
            )


class URLImporter(Importer):
    """ Common ancestor for URL-based imports """
    
    def _url(self, url):
        """ Tryies to read url, returns content or empty string """
        try:
            req = urllib.request.urlopen(url)
            return req.read()
        except:
            return ""

    def load_source(self, source):
        self.source_code = self._url(source)

    def load_json(self, url):
        try:
            return json.loads(self._url(url).decode('utf-8'))
        except:
            return None


class RedmineImporter(URLImporter):

    def interested(self, source):
        """ Interested in redmine projects """
        if re.match( r'https://.*/projects/.*', source):

            # find redmine fingerprint in source
            self.load_source(source)
            if (b"/redmine_agile/" in self.source_code):
                return True

        return False

    def get_whips(self, source, ask_for_credentials=True):
        """ Returns all issues from redmine project """
        self.base_url = source + '/issues.json'
        resp = self.load_json(self.base_url)
        ret = []
        if resp:
            original_count = resp['total_count']
            if original_count:
                offset= 0
                while offset < original_count: 
                    resp = self.load_json(self.base_url + '?limit=100&amp;offset=%s' % offset)
                    offset += 100
                    for x in resp['issues']:
                        whip = models.Whip()
                        myline = x['subject']
                        whip.message = myline
                        ret.append(whip)
        return ret

    def sync_up(self, whip):
        return False

    def excuse(self):
        return """Analyzed {url}, but no issues found.""".format(url=self.base_url)


class GitlabImporter(URLImporter):
    """ Import issues from gitlab.com via their api v4
    """

    def __init__(self):
        self.excuse_reason = None

    def interested(self, source):
        """ Interested in every URL in form 
            https://gitlab.(subdomain)/(tld)/username/project/some_stuff
            That means user can enter any regular URL from within the project
        """
        m = re.search( r'https://gitlab.(com|[a-zA-Z0-9_\-]{1,30}\.[a-zA-Z]{2,10})/([a-zA-Z0-9_\-]*)/([a-zA-Z0-9_\-\.]*)(|/.*)', source)
        if m:
            self.gitlab_domain = m[1]
            self.gitlab_user = m[2]
            self.gitlab_project = m[3]
            return True
        
        return False

    def _auth_headers(self, url):
        # returns authorization headers from saved GitLab token
        _, token = func.credentials_from_file(url)
        return {"Private-Token": token } if token else {}

    def load_json_spec(self, url):
        """ Tryies to read url, returns tuple: (content, response_code, total_number_of_items)
            or (None, None) if something goes wrong
        """
        try:
            response = requests.get(url, headers=self._auth_headers(url))
        except requests.exceptions.ConnectionError:
            return None, None, None
            
        try:
            total = int(response.headers['X-total'])
        except:
            total = None

        return response.json(), response.status_code, total

    def get_whips(self, source, ask_for_credentials=True):
        """ Returns open issues from project """
        domain = "https://gitlab.%s" % self.gitlab_domain
        self.base_url = "%s/api/v4/projects/%s%%2F%s/issues" % (domain, self.gitlab_user, self.gitlab_project)
        ret, page = [], 1
        while True:
            resp, status, total = self.load_json_spec(self.base_url + '?per_page=100&amp;page=%s' % page)

            # Complain if response problems (mostly authorization) occur
            if status in (401, 404):
                if ask_for_credentials:
                    print("""Server error %s (kind of unauthorized).""" % status)
                    answer = input("Enter API token (will be saved in credencials file). [Enter] to skip:")
                    if answer:
                        func.credentials_to_file("", answer, source)
                        continue
                    else:
                        self.excuse_reason = "Unauthorized"
                        break
                else:
                    self.excuse_reason = "Unauthorized"
                    break
                
            if status in range (400,500):
                try:
                    self.excuse_reason = """Server error %s: %s""" % (status, resp['error_description'])
                except KeyError:
                    self.excuse_reason = """Server error %s.""" % status
                break
        
            if not resp:
                break

            for x in resp:
                if x['state']=='opened':
                    whip = models.Whip()
                    myline = x['title']
                    whip.message = myline
                    
                    whip.sync_data = {
                        'source': domain,
                        'user': self.gitlab_user,
                        'project': self.gitlab_project,
                        'issue_id': x['iid'], # internal id of issue
                    }
                    
                    ret.append(whip)

            if page*100 >= total:
                break
            page += 1
        return ret

    def sync_up(self, whip):

        url = "%s/api/v4/projects/%s%%2F%s/issues/%s" % \
            (whip.sync_data['source'], whip.sync_data['user'], 
            whip.sync_data['project'], whip.sync_data['issue_id'])

        payload = { 'title': whip.message }

        try:
            response = requests.put(url, headers=self._auth_headers(url), data=payload)
        except requests.exceptions.ConnectionError:
            print("Connection eror while syncing")
            return False
           
        if response.status_code == 200:
            return True
        else:
            print("Server returned error %s, whip was not synced" % response.status_code)
            return False

    def excuse(self):
        return self.excuse_reason or """Analyzed {url}, but no open issues found.""".format(url=self.base_url)


class HTMLImporter(URLImporter):

    def interested(self, source):
        """ Interested in everything that looks like HTML page """
        self.load_source(source)
        return True if self.source_code else False

    def get_whips(self, source, ask_for_credentials=True):
        """ Returns content of <li> tags, stripped, file endings replaced with spaces """
        soup = BeautifulSoup(self.source_code, 'html.parser')
        items = soup.find_all("li")
        items = [ item.text.strip() for item in items if item.text.strip()]

        ret = []
        for x in items:
            whip = models.Whip()
            myline = x.replace("\n", " ").replace("\r", " ") 
            whip.message = myline
            ret.append(whip)
        return ret

    def sync_up(self, whip):
        return False

    def excuse(self):
        return """Analyzed {count} bytes, but no list items (<li> tags) found in URL.""".format(
            count=len(self.source_code))

